import Firebase from 'firebase/app';
import 'firebase/firestore';
import 'firebase/auth';

const config = {
  apiKey: 'AIzaSyAHe8q5K0OSQlfAW8naWqUGigyoimmL3Dc',
  authDomain: 'instagram-dc376.firebaseapp.com',
  projectId: 'instagram-dc376',
  storageBucket: 'instagram-dc376.appspot.com',
  messagingSenderId: '507638915946',
  appId: '1:507638915946:web:bb984df25a3bba518a3b13'
};

const firebase = Firebase.initializeApp(config);
const { FieldValue } = Firebase.firestore;

export { firebase, FieldValue };
